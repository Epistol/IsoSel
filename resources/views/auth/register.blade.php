@extends('layouts.app')

@section('title', 'Inscription')

@section('content')



    <!--suppress ALL -->
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Inscription</div>
                    <div class="panel-body">
                        <form class="form-horizontal" id='demo-form' role="form" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nom</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Adresse e-mail</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            {{-- <div class="form-group{{ $errors->has('truc') ? ' has-error' : '' }}">

                                 <label for="truc" class="col-md-4 control-label">Collec</label>
                                 <div class="col-md-6">
                                     <p class="control">
                                         @foreach($collec as $c)
                                             <label class="checkbox">
     <input type="checkbox">
                                                 {{$c}}
   </label>

                                         @endforeach
                                     </p>
                                 </div>
                             </div>--}}


                            <div class="form-group{{ $errors->has('ddn') ? ' has-error' : '' }}">
                                <label for="ddn" class="col-md-4 control-label">Votre date de naissance</label>

                                <div class="col-md-6">
                                    <input id="ddn" type="date" class="form-control" name="ddn" value="{{ old('ddn') }}" required>

                                    @if ($errors->has('ddn'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('ddn') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Mot de passe</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirmez votre mot de passe</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <input id="auto" placeholder="TETS">



                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">

                                    <button  class="{{--g-recaptcha--}} btn btn-primary"
                                             data-sitekey="6LfhWxYUAAAAACTCsOIw8a_knLNCVDx1vA7JW2SR"
                                             data-callback="onSubmit" type="submit" >
                                        C'est parti !
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {


            // Autocompletion des origines
            $("#auto").autocomplete({

                source: function (request, response) {
                    $.ajax({
                        type: "get",
                        minLength: 1,
                        url: "/api/villes/?ville=" + encodeURIComponent(request.term),
                        dataType: "json",
                        success: function (data) {
                            response(data);
                            console.log(data);
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { console.log(errorThrown)
                        }
                    });
                },

            });
        });

    </script>





@endsection


