<?php




Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/competences/', 'Competence\CompetenceController@index')->name('Competences_Index');
Route::get('/competences/ajout', 'Competence\CompetenceController@create')->name('Competences_Ajout');
Route::post('/competences/ajout', 'Competence\CompetenceController@store')->name('Competences_Ajout');

Route::get('/home', 'HomeController@index');
