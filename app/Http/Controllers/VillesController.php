<?php

namespace IsoSel\Http\Controllers;

use Illuminate\Http\Request;

class VillesController extends Controller
{
    public function ville_get(Request $request){
	    $truc = $request->request;
	    foreach ($truc as $t){
		    $ville = $t;
	    }

	    $var = 'https://datanova.legroupe.laposte.fr/api/records/1.0/search/?dataset=base-officielle-des-codes-postaux-croisee-avec-geoflar-communes-2013&lang=fr'. '&q='. $ville;
	    $json =  json_decode(file_get_contents($var), true);
	    foreach ($json['records'] as $enregistrement){
		    $nom = $enregistrement['fields']['nom_de_la_commune'];
		    $noms[] = $nom;
	    }

	    return json_encode($noms);

	    /*	return json_encode($json);*/
    }
}
